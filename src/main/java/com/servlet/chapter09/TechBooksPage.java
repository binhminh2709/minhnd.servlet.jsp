package com.servlet.chapter09;

import javax.servlet.annotation.WebServlet;

/** A specialization of the CatalogPage servlet that displays a page selling two famous computer books.
 *  Orders are sent to the OrderPage servlet.
 *  Sai mot cai webservlet la sai het, hic
 */

@WebServlet("/tech_books_page")
public class TechBooksPage extends CatalogPage {
  
  private static final long serialVersionUID = 1157119192248889034L;
  
  public void init() {
    String[] ids = { "hall001", "hall002" };
    setItems(ids);
    setTitle("All-Time Best Computer Books");
  }
}
