package com.servlet.chapter05;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utility.GzipUtilities;

/** Servlet with <B>long</B> output. Used to test
 *  the effect of the gzip compression.
 */

@WebServlet("/long-servlet")
public class LongServlet extends HttpServlet {
  
  /**
   * 
   */
  private static final long serialVersionUID = 9090301525894210152L;
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    
    // Change the definition of "out" depending on whether or not gzip is supported.
    PrintWriter out;
    if (GzipUtilities.isGzipSupported(request) && !GzipUtilities.isGzipDisabled(request)) {
      System.out.println("brower support Gzip and enable Gzip");
      out = GzipUtilities.getGzipWriter(response);
      response.setHeader("Content-Encoding", "gzip");
    } else {
      System.out.println("brower don't support Gzip and enable Gzip");
      out = response.getWriter();
    }
    
    // Once "out" has been assigned appropriately, the rest of the page has no dependencies on the type of Writer being used.
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
    String title = "Long Page";
    StringBuilder sHtml = new StringBuilder();
    sHtml.append("<HTML>\n");
    sHtml.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    sHtml.append("<BODY BGCOLOR=\"#FDF5E6\">\n");
    sHtml.append("<H1 ALIGN=\"CENTER\">" + title + "</H1>\n");
    
    out.println(docType + sHtml.toString());
    String line = "Blah, blah, blah, blah, blah. " + "Yadda, yadda, yadda, yadda.";
    for (int i = 0; i < 10000; i++) {
      out.println(line);
    }
    out.println("</BODY></HTML>");
    out.close(); // Needed for gzip; optional otherwise.
  }
}
