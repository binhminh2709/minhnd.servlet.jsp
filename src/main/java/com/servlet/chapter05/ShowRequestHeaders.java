package com.servlet.chapter05;

import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.*;
import javax.servlet.http.*;
import java.util.*;

/** Shows all the request headers sent on the current request.
 *  <p>
 *  From <a href="http://courses.coreservlets.com/Course-Materials/">the
 *  coreservlets.com tutorials on servlets, JSP, Struts, JSF, Ajax, GWT, and Java</a>.
 */

@WebServlet("/show-request-headers")
public class ShowRequestHeaders extends HttpServlet {
  
  /**
   * 
   */
  private static final long serialVersionUID = 2757025521879638839L;
  
  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String title = "Servlet Example: Showing Request Headers";
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
    StringBuilder sHtml = new StringBuilder();
    sHtml.append("<HTML>\n");
    sHtml.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    sHtml.append("<BODY BGCOLOR=\"#FDF5E6\">\n");
    sHtml.append("<H1 ALIGN=\"CENTER\">" + title + "</H1>\n");
    sHtml.append("<B>Request Method: </B> ");
    sHtml.append(request.getMethod() + "<BR>\n");
    sHtml.append("<B>Request URI: </B>");
    sHtml.append(request.getRequestURI() + "<BR>\n");
    sHtml.append("<B>Request Protocol: </B>");
    sHtml.append(request.getProtocol() + "<BR><BR>\n");
    sHtml.append("<TABLE BORDER=1 ALIGN=\"CENTER\">\n");
    sHtml.append("<TR BGCOLOR=\"#FFAD00\">\n");
    sHtml.append("<TH>Header Name<TH>Header Value");
    sHtml.append("");
    
    out.println(docType + sHtml.toString());
    
    Enumeration<String> headerNames = request.getHeaderNames();
    while (headerNames.hasMoreElements()) {
      String headerName = headerNames.nextElement();
      out.println("<TR><TD>" + headerName);
      out.println("<TD>" + request.getHeader(headerName));
    }
    out.println("</TABLE>\n</BODY></HTML>");
  }
  
  /** Since this servlet is for debugging, have it
   *  handle GET and POST identically.
   */
  
  @Override
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }
}
