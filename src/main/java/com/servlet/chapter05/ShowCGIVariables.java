package com.servlet.chapter05;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** Creates a table showing the current value of each
 *  of the standard CGI variables.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages 2nd Edition
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2003 Marty Hall; may be freely used or adapted.
 */
@WebServlet("/show-CGI-Variables")
public class ShowCGIVariables extends HttpServlet {
  /**
   * 
   */
  private static final long serialVersionUID = 252421735759358410L;
  
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String[][] variables = { { "AUTH_TYPE", request.getAuthType() }, { "CONTENT_LENGTH", String.valueOf(request.getContentLength()) },
        { "CONTENT_TYPE", request.getContentType() }, { "DOCUMENT_ROOT", getServletContext().getRealPath("/") },
        { "PATH_INFO", request.getPathInfo() }, { "PATH_TRANSLATED", request.getPathTranslated() }, { "QUERY_STRING", request.getQueryString() },
        { "REMOTE_ADDR", request.getRemoteAddr() }, { "REMOTE_HOST", request.getRemoteHost() }, { "REMOTE_USER", request.getRemoteUser() },
        { "REQUEST_METHOD", request.getMethod() }, { "SCRIPT_NAME", request.getServletPath() }, { "SERVER_NAME", request.getServerName() },
        { "SERVER_PORT", String.valueOf(request.getServerPort()) }, { "SERVER_PROTOCOL", request.getProtocol() },
        { "SERVER_SOFTWARE", getServletContext().getServerInfo() } };
    String title = "Servlet Example: Showing CGI Variables";
    StringBuffer sDocType = new StringBuffer();
    sDocType.append("<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 ");
    sDocType.append("Transitional//EN\">\n");
    sDocType.append("<HTML>\n");
    sDocType.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    sDocType.append("<BODY BGCOLOR=\"#FDF5E6\">\n");
    sDocType.append("<CENTER>\n");
    sDocType.append("<H1>" + title + "</H1>\n");
    sDocType.append("<TABLE BORDER=1>\n");
    sDocType.append("<TR BGCOLOR=\"#FFAD00\">\n");
    sDocType.append("<TH>CGI Variable Name<TH>Value");
    sDocType.append("");
    sDocType.append("");
    out.println(sDocType.toString());
    
    for (int i = 0; i < variables.length; i++) {
      String varName = variables[i][0];
      String varValue = variables[i][1];
      if (varValue == null)
        varValue = "<I>Not specified</I>";
      out.println("<TR><TD>" + varName + "<TD>" + varValue);
    }
    out.println("</TABLE></CENTER></BODY></HTML>");
  }
  
  /** POST and GET requests handled identically. */
  
  public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    doGet(request, response);
  }
}
