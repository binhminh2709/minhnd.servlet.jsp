/* SQL script to create music table.
 *
 * From MySQL monitor run:
 *   mysql> SOURCE create_music_table.sql
 *
 * From Oracle9i SQL*Plus run:
 *   SQL> START create_music_table.sql
 *
 * In both cases, you may need to specify the full
 * path to the SQL script.
 *  
 * Taken from Core Servlets and JavaServer Pages 2nd Edition
 * from Prentice Hall and Sun Microsystems Press,
 * http://www.coreservlets.com/.
 * &copy; 2003 Marty Hall and Larry Brown.
 * May be freely used or adapted. 
 */

DROP TABLE music;
CREATE TABLE music (
  id INTEGER,
  composer VARCHAR(16),
  concerto VARCHAR(24),
  available INTEGER,
  price FLOAT);
INSERT INTO music 
  VALUES (1, 'Mozart', 'No. 21 in C# minor', 7, 24.99);
INSERT INTO music 
  VALUES (2, 'Beethoven', 'No. 3 in C minor', 28, 10.99);
INSERT INTO music
  VALUES (3, 'Beethoven', 'No. 5 Eb major', 33, 10.99);
INSERT INTO music 
  VALUES (4, 'Rachmaninov', 'No. 2 in C minor', 9, 18.99);
INSERT INTO music
  VALUES (5, 'Mozart', 'No. 24 in C minor', 11, 21.99);
INSERT INTO music
  VALUES (6, 'Beethoven', 'No. 4 in G', 33, 12.99);
INSERT INTO music 
  VALUES (7, 'Liszt', 'No. 1 in Eb major', 48, 10.99);
COMMIT;  