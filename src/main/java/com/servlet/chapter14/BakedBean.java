package com.servlet.chapter14;

/** Small bean to illustrate various bean-sharing mechanisms.
 */

public class BakedBean {
  
  private String level = "half-baked";
  private String goesWith = "hot dogs";
  
  public String getLevel() {
    return (level);
  }
  
  public void setLevel(String newLevel) {
    level = newLevel;
  }
  
  public String getGoesWith() {
    return (goesWith);
  }
  
  public void setGoesWith(String dish) {
    goesWith = dish;
  }
}
