package com.servlet.chapter08;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utility.CookieUtilities;
import com.utility.LongLivedCookie;

/** A variation of the RepeatVisitor servlet that uses
 *  CookieUtilities.getCookieValue and LongLivedCookie to simplify the code.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages 2nd Edition
 *  from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2003 Marty Hall; may be freely used or adapted.
 */

@WebServlet("/repeat_visitor2")
public class RepeatVisitor2 extends HttpServlet {

  private static final long serialVersionUID = -8991957148917361932L;

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    boolean newbie = true;
    String value = CookieUtilities.getCookieValue(request, "repeatVisitor2", "no");
    if (value.equals("yes")) {
      newbie = false;
    }
    String title;
    if (newbie) {
      LongLivedCookie returnVisitorCookie = new LongLivedCookie("repeatVisitor2", "yes");
      response.addCookie(returnVisitorCookie);
      title = "Welcome Aboard";
    } else {
      title = "Welcome Back";
    }
    response.setContentType("text/html");
    PrintWriter out = response.getWriter();
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
    StringBuilder sHtml = new StringBuilder();
    sHtml.append("<HTML>\n");
    sHtml.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    sHtml.append("<BODY BGCOLOR=\"#FDF5E6\">\n" + "<H1 ALIGN=\"CENTER\">" + title + "</H1>\n" + "</BODY>");
    sHtml.append("</HTML>");
    out.println(docType + sHtml.toString());
  }
}
