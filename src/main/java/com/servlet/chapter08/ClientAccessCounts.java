package com.servlet.chapter08;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.utility.CookieUtilities;
import com.utility.LongLivedCookie;

/** Servlet that prints per-client access counts.
 *  <P>
 *  Taken from Core Servlets and JavaServer Pages 2nd Edition from Prentice Hall and Sun Microsystems Press,
 *  http://www.coreservlets.com/.
 *  &copy; 2003 Marty Hall; may be freely used or adapted.
 */
/** Servlet that prints per-client access counts. */
@WebServlet("/client_access_counts")
public class ClientAccessCounts extends HttpServlet {
  private static final long serialVersionUID = 6665350051940454866L;

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String countString = CookieUtilities.getCookieValue(request, "accessCount", "1");
    int count = 1;
    try {
      count = Integer.parseInt(countString);
    } catch (NumberFormatException nfe) {
    }
    LongLivedCookie c = new LongLivedCookie("accessCount", String.valueOf(count + 1));
    response.addCookie(c);
    response.setContentType("text/html");
    
    PrintWriter out = response.getWriter();
    String title = "Access Count Servlet";
    String docType = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0 " + "Transitional//EN\">\n";
    
    StringBuilder sHtml = new StringBuilder();
    sHtml.append("<HTML>\n");
    sHtml.append("<HEAD><TITLE>" + title + "</TITLE></HEAD>\n");
    sHtml.append("<BODY BGCOLOR=\"#FDF5E6\">\n");
    sHtml.append("<CENTER>\n");
    sHtml.append("<H1>" + title + "</H1>\n");
    sHtml.append("<H2>This is visit number " + count + " by this browser.</H2>\n");
    sHtml.append("</CENTER>");
    sHtml.append("</BODY></HTML>");
    
    out.println(docType + sHtml.toString());
  }
}
