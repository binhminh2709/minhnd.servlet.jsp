package com.servlet.chapter08;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/** Reads firstName and lastName request parameters and forwards
 *  to JSP page to display them. Uses session-based bean sharing
 *  to remember previous values.
 */

/** Servlet that processes a registration form containing a user's first name, last name, and email address.
 * If all the values are present, the servlet displays the values.
 * If any of the values are missing, the input form is redisplayed.
 * Either way, the values are put into cookies so that the input form can use the previous values.
 */
@WebServlet("/registration_servlet")
public class RegistrationServlet extends HttpServlet {
  private static final long serialVersionUID = -7072481461874676621L;

  @Override
  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    NameBean nameBean = (NameBean) session.getAttribute("nameBean");
    if (nameBean == null) {
      String firstName = request.getParameter("firstName");
      String lastName = request.getParameter("lastName");
      nameBean = new NameBean(firstName, lastName);
      session.setAttribute("nameBean", nameBean);
    }
    String address = "/chap08/ShowName.jsp";
    RequestDispatcher dispatcher = request.getRequestDispatcher(address);
    dispatcher.forward(request, response);
  }
}
