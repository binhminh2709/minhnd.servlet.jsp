
package com.servlet.chapter15;

import java.math.BigInteger;

import com.servlet.chapter07.Primes;

public class PrimeBean {
  
  private BigInteger prime;
  
  public PrimeBean(String lengthString) {
    int length = 150;
    try {
      length = Integer.parseInt(lengthString);
    } catch (NumberFormatException nfe) {
    }
    setPrime(Primes.nextPrime(Primes.random(length)));
  }
  
  public BigInteger getPrime() {
    return (prime);
  }
  
  public void setPrime(BigInteger newPrime) {
    prime = newPrime;
  }
}
