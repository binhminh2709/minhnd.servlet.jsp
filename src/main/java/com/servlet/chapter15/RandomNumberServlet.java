
package com.servlet.chapter15;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** Servlet that generates a random number, stores it in a bean, and forwards to JSP page to display it.
 */

@WebServlet("/chap15_random_number_servlet")
public class RandomNumberServlet extends HttpServlet {
  
  private static final long serialVersionUID = 891638821889603574L;

  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    NumberBean bean = new NumberBean(Math.random());
    request.setAttribute("randomNum", bean);
    String address = "/chap15/mvc-sharing/RandomNum.jsp";
    RequestDispatcher dispatcher = request.getRequestDispatcher(address);
    dispatcher.forward(request, response);
  }
}
