
package com.servlet.chapter15;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Reads firstName and lastName request parameters and forwards to JSP page to display them.
 * Uses session-based bean sharing to remember previous values.
 */

@WebServlet("/chap15_registration_servlet")
public class RegistrationServlet extends HttpServlet {
  
  private static final long serialVersionUID = 5177255712970362119L;

  public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    HttpSession session = request.getSession();
    NameBean nameBean = (NameBean) session.getAttribute("nameBean");
    if (nameBean == null) {
      String firstName = request.getParameter("firstName");
      String lastName = request.getParameter("lastName");
      nameBean = new NameBean(firstName, lastName);
      session.setAttribute("nameBean", nameBean);
    }
    String address = "/chap15/mvc-sharing/ShowName.jsp";
    RequestDispatcher dispatcher = request.getRequestDispatcher(address);
    dispatcher.forward(request, response);
  }
}
