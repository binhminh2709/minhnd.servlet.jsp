
package com.servlet.chapter15;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/chap15_request_forwarding_snippet")
public class RequestForwardingSnippet extends HttpServlet {
  
  private static final long serialVersionUID = -5358482872397319838L;
  
  public void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
    String operation = request.getParameter("operation");
    if (operation == null) {
      operation = "unknown";
    }
    String address;
    if (operation.equals("order")) {
      address = "/chap15/Order.jsp";
    } else if (operation.equals("cancel")) {
      address = "/chap15/Cancel.jsp";
    } else {
      address = "/chap15/UnknownOperation.jsp";
    }
    RequestDispatcher dispatcher = request.getRequestDispatcher(address);
    dispatcher.forward(request, response);
  }
}
