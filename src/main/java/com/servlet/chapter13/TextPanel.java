
package com.servlet.chapter13;

import java.awt.BorderLayout;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

/** JPanel that places a panel with text drawn at various angles
 *  in the top part of the window and a JComboBox containing
 *  font choices in the bottom part.
 */

public class TextPanel extends JPanel implements ActionListener {
  
  /**
   * 
   */
  private static final long serialVersionUID = 4401273570526417399L;
  private JComboBox<String>    fontBox;
  private DrawingPanel drawingPanel;
  
  public TextPanel() {
    GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
    String[] fontNames = env.getAvailableFontFamilyNames();
    fontBox = new JComboBox<String>(fontNames);
    
    setLayout(new BorderLayout());
    
    JPanel fontPanel = new JPanel();
    fontPanel.add(new JLabel("Font:"));
    fontPanel.add(fontBox);
    
    JButton drawButton = new JButton("Draw");
    drawButton.addActionListener(this);
    
    fontPanel.add(drawButton);
    
    add(fontPanel, BorderLayout.SOUTH);
    
    drawingPanel = new DrawingPanel();
    fontBox.setSelectedItem("Serif");
    drawingPanel.setFontName("Serif");
    add(drawingPanel, BorderLayout.CENTER);
  }
  
  public void actionPerformed(ActionEvent e) {
    drawingPanel.setFontName((String) fontBox.getSelectedItem());
    drawingPanel.repaint();
  }
}
