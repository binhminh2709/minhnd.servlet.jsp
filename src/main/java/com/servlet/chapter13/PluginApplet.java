package com.servlet.chapter13;

import javax.swing.JApplet;

import com.utility.WindowUtilities;

/** An applet that uses Swing and Java 2D and thus requires the Java Plug-In.
 */

public class PluginApplet extends JApplet {
  /**
   * 
   */
  private static final long serialVersionUID = -4589065398180874507L;

  public void init() {
    WindowUtilities.setNativeLookAndFeel();
    setContentPane(new TextPanel());
  }
}
