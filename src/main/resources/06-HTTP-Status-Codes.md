6.1 Specifying Status Codes

When they do want to, they use
- response.setStatus,
- response.sendRedirect,
- response.sendError.

6.2 HTTP 1.1 Status Codes
verified by checking request.getRequestProtocol().

These codes fall into five general categories
• 100–199: Codes in the 100s are informational, indicating that the client should respond with some other action.
• 200–299: Values in the 200s signify that the request was successful.
• 300–399: Values in the 300s are used for files that have moved and usually include a Location header indicating the new address.
• 400–499: Values in the 400s indicate an error by the client.
• 500–599: Codes in the 500s signify an error by the server.


100 (Continue)
If the server receives an Expect request header with a value of 100-continue, it means that the client is asking if it can send an attached document in a follow-up request. In such a case, the server should either respond with status 100 (SC_CONTINUE) to tell the client to go ahead or use 417 (SC_EXPECTATION_FAILED) to tell the browser it won’t accept the document.
This status code is new in HTTP 1.1.

200 (OK)
A value of 200 (SC_OK) means that everything is fine; the document follows for GET and POST requests. This status is the default for servlets; if you don’t use setStatus, you’ll get 200.

202 (Accepted)
A value of 202 (SC_ACCEPTED) tells the client that the request is being acted upon but processing is not yet complete.

204 (No Content)
A status code of 204 (SC_NO_CONTENT) stipulates that the browser should continue to display the previous document because no new document is available. This behavior is useful if the user periodically reloads a page by pressing the Reload button and you can determine that the previous page is already up-to-date.

205 (Reset Content)
A value of 205 (SC_RESET_CONTENT) means that there is no new document but the browser should reset the document view. Thus, this status code is used to instruct browsers to clear form fields. It is new in HTTP 1.1.

301 (Moved Permanently)
The 301 (SC_MOVED_PERMANENTLY) status indicates that the requested document is elsewhere; the new URL for the document is given in the Location response header. Browsers should automatically follow the link to the new URL.


302 (Found)
This value is similar to 301, except that in principle the URL given by the Location header should be interpreted as a temporary replacement, not a permanent one. In practice, most browsers treat 301 and 302 identically.
Note: in HTTP 1.0, the message was Moved Temporarily instead of Found, and the constant in HttpServletResponse is SC_MOVED_TEMPORARILY, not the expected SC_FOUND.

So why use a servlet at all? Redirects are useful for the following tasks:

• Computing destinations. If you know the final destination for the user in advance, your hypertext link or HTML form could send the user directly there. But, if you need to look at the data before deciding where to obtain the necessary results, a redirection is useful.
  
  For example, you might want to send users to a standard site that gives information on stocks, but you need to look at the stock symbol before deciding whether to send them to the New York Stock Exchange, NASDAQ, or a non-U.S. site.
  
• Tracking user behavior. If you send users a page that contains a hypertext link to another site, you have no way to know if they 
actually click on the link. But perhaps this information is important in analyzing the usefulness of the different links you send them. So, instead of sending users the direct link, you can send them a link to your own site, where you can then record some information and then redirect them to the real site.

  For example, several search engines use this trick to determine which of the results they display are most popular.
  
• Performing side effects. What if you want to send users to a certain site but set a cookie on the user’s browser first?
No problem: return both a Set-Cookie response header (by means of response.addCookie—see Chapter 8) and a 302 status code (by means of response.sendRedirect).

303 (See Other)
The 303 ( SC_SEE_OTHER ) status is similar to 301 and 302, except that if the original request was POST , the new document (given in the Location header) should be retrieved with GET . See status code 307. This code is new in HTTP 1.1.

304 (Not Modified)
When a client has a cached document, it can perform a conditional request by supplying an If-Modified-Since header to signify that it wants the document only if it has been changed since the specified date. A value of 304 ( SC_NOT_MODIFIED ) means that the cached version is up-to-date and the client should use it. Otherwise, the server should return the requested document with the normal (200) status code. Servlets normally should not set this status code directly. Instead, they should implement the getLastModified method and let the default service method handle conditional requests based upon this modification date. For an example, see the LotteryNumbers servlet in Section 3.6 (The Servlet Life Cycle).

































