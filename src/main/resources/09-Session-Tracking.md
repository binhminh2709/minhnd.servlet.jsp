9.1 The Need for Session Tracking
- Cookies
Using cookies in this manner is an excellent solution and is the most widely used approach for session handling. Still, it is nice that servlets have a higher-level API that handles all this plus the following tedious tasks:
• Extracting the cookie that stores the session identifier from the other cookies (there may be many cookies, after all).
• Determining when idle sessions have expired, and reclaiming them.
• Associating the hash tables with each request.
• Generating the unique session identifiers.

- URL Rewriting.
- Hidden Form Fields.

why use Session Tracking in Servlets.

9.2 Session Tracking Basics.
  - Accessing the session object associated with the current request. Call request.getSession to get an HttpSession
object, which is a simple hash table for storing user-specific data.

  - Looking up information associated with a session. Call getAttribute on the HttpSession object, cast the return value to the appropriate type, and check whether the result is null.

  - Storing information in a session. Use setAttribute with a key and a value.

  - Discarding session data. Call removeAttribute to discard a specific value. Call invalidate to discard an entire session. Call logout to log the client out of the Web server and invalidate all sessions associated with that user.

1. Accessing the Session Object Associated with the Current Request
2. Looking Up Information Associated with a Session
3. Associating Information with a Session
  Session attributes merely have to be of type Object (i.e., they can be anything other than null or a primitive like int, double, or boolean). However, some application servers support distributed Web applications in which an application is shared across a cluster of physical servers. Session tracking needs to still work in such a case, so the system needs to be able to move session objects from machine to machine. Thus, if you run in such an environment and you mark your Web application as being distributable, you must meet the additional requirement that session attributes implement the Serializable interface.
  
4. Discarding Session Data: three options.
• Remove only the data your servlet created.
Call removeAttribute("key")  to discard the value associated with the specified key. This is the most common approach.

• Delete the whole session (in the current Web application).
Can call invalidate to discard an entire session. Just remember that doing so causes all of that user’s session data to be lost, not just the session data that your servlet or JSP page created. So, a l l the sevlets and JSP pages in a Web application have to agree on the cases for which invalidate may be called.

• Log the user out and delete all sessions belonging to him or her.
Finally, in servers that support servlets 2.4 and JSP 2.0, you can call logout to log the client out of the Web server and invalidate all sessions (at most one per Web application) associated with that user.
Again, since this action affects servlets other than your own, be sure to coordinate use of the logout command with the other developers at your site.

9.3 The Session-Tracking API

public void invalidate()
public void logout()
public boolean isNew()
public int getMaxInactiveInterval()
public void setMaxInactiveInterval(int seconds)

9.4 Browser Sessions vs. Server Sessions
  By default, session-tracking is based on cookies that are stored in the browser’s memory, not written to disk. Thus, unless the servlet explicitly reads the incoming JSESSIONID cookie, sets the maximum age and path, and sends it back out, quitting the browser results in the session being broken: the client will not be able to access the session again. The problem, however, is that the server does not know that the browser was closed and thus the server has to maintain the session in memory until the inactive interval has been exceeded.
  
  The analogous situation in the servlet world is one in which the server is trying to decide if it can throw away your HttpSessionobject. Just because you are not currently using the session does not mean the server can throw it away.
Maybe you will be back (submit a new request) soon? If you quit your browser, thus causing the browser-session-level cookies to be lost, the session is effectively broken. But, as with the case of getting in your car and leaving Wal-Mart, the server does not know that you quit your browser. So, the server still has to wait for a period of time to see if the session has been abandoned. Sessions automatically become inactive when the amount of time between client accesses exceeds the interval specified by getMaxInactiveInterval. When this happens, objects stored in the HttpSessionobject are removed (unbound). Then, if those objects implement the HttpSessionBindingListener interface, they are automatically notified. The one exception to the “the server waits until sessions time out” rule is if invalidateor logoutis called. This is akin to your explicitly telling the Wal-Mart clerk that you are leaving, so the server can immediately remove all the items from the session and destroy the session object.

9.5 Encoding URLs Sent to the Client
+ The first one is where the URLs are embedded in the Web page that the servlet generates. These ULs should be passed through the encodeURL method of HttpServletResponse. The method determines if URL rewriting is currently in
use and appends the session information only if necessary. The URL is returned unchanged otherwise.

+ The second situation in which you might use a URL that refers to your own site is in a sendRedirect call (i.e., placed into the Location response header). In this second situation, different rules determine whether session information needs to be attached, so you cannot use encodeURL. Fortunately, HttpServletResponse supplies an encodeRedirectURL method to handle that case

9.6 A Servlet That Shows Per-Client Access Counts.

9.7 Accumulating a List of User Data

