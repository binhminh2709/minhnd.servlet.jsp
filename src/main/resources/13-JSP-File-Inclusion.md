Think of the include directive as a preprocessor: the included file is inserted character for character into the main page, then the resultant page is treated as a single JSP page.
So, the fundamental difference between jsp:includeand the include directive is the time at which they are invoked: jsp:includeis invoked at request time, whereas the include directive is invoked at page translation time.

However, there are more implications of this difference than you might first think

Differences Between jsp:include and the include Directive

Core Approach
For file inclusion, use jsp:includewhenever possible. Reserve the include directive (<%@ include ... %>) for cases in which the included file defines fields or methods that the main page uses or when the included file sets response headers of the main page.


13.4 Including Applets for the Java Plug-In.

