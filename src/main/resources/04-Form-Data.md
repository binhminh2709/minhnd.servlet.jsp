1. Use the FORM element to create an HTML form. <FORM ACTION="...">...</FORM>
2. Use input elements to collect user data. <INPUT TYPE="TEXT" NAME="...">
3. Place a submit button near the bottom of the form. <INPUT TYPE="SUBMIT">

Extracting the needed information from this form data is traditionally one of the most tedious parts of server-side programming.
	
First of all, before servlets you generally had to read the data one way for GET requests (in traditional CGI, this is usually through the QUERY_STRING environment variable) and a different way for POST requests (by reading the standard input in traditional CGI).

Second, you have to chop the pairs at the ampersands, then separate the parameter names (left of the equal signs) from the parameter values (right of the equal signs).

Third, you have to URL-decode the values: reverse the encoding that the browser uses on certain characters. Alphanumeric characters are sent unchanged by the browser, but spaces are converted to plus signs and other characters are converted to %XX, where XX is the ASCII (or ISO Latin-1) value of the character, in hex.

For example, if someone enters a value of “ ~hall, ~gates, and ~mcnealy ” into a textfield with the name 
users in an HTML form , the data is sent as “users=%7Ehall%2C+%7Egates%2C+and+%7Emcnealy”, and the 
server-side program has to reconstitute the original string.

the fourth reason that it is tedious to parse form data with traditional server-side technologies is that values can be omitted (e.g., “ param1=val1&param2=&param3=val3 ”) or a parameter can appear more than once (e.g., “ param1=val1&param2=val2&param1=val3 ”), so your parsing code needs special cases for these situations.


Reading Form Data from Servlets
1- Reading Single Values: getParameter
2- Reading Multiple Values: getParameterValues
3- Looking Up Parameter Names: getParameterNames and getParameterMap
4- Reading Raw Form Data and Parsing Uploaded Files: getReader or getInputStream
5- Reading Input in Multiple Character Sets: setCharacterEncoding


Redisplay Options
1.Have the same servlet present the form, process the data, and present the results.
2.Have one servlet present the form; have a second servlet process the data and present the results.
3.Have a JSP page “manually” present the form; have a servlet or JSP page process the data and present the results.
4.Have a JSP page present the form, automatically filling in the fields with values obtained from a data object. Have a servlet or JSP page process the data and present the results.
