<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ContactSection</title>
</head>
<body>
<%@ page import="java.util.Date" %>
<%-- The following become fields in each servlet that results from a JSP page that includes this file. --%>
<%!
private int accessCount = 0;
private Date accessDate = new Date();
private String accessHost = "<I>No previous access</I>";
%>
<p>
<hr>
This page &copy; 2003 <a href="http//www.my-company.com/">my-company.com</a>.
This page has been accessed <%= ++accessCount %> times since server reboot.
It was most recently accessed from <%= accessHost %> at <%= accessDate %>.
<% accessHost = request.getRemoteHost(); %>
<% accessDate = new Date(); %>
</body>
</html>