<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>Accessing Bean Properties</TITLE>
<LINK REL=STYLESHEET HREF="/minhnd.servlet.jsp/chap16//el/JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Accessing Bean Properties</TH></TR>
  </TABLE>
  <P>
  <UL>
    <LI><B>First Name:</B> ${employee.name.firstName}</LI>
    <LI><B>Last Name:</B> ${employee.name.lastName}</LI>
    <LI><B>Company Name:</B> ${employee.company.companyName}</LI>
    <LI><B>Company Business:</B> ${employee.company.business}</LI>
  </UL>
</BODY>
</HTML>