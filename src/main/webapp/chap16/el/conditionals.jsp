<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
<TITLE>Conditional Evaluation</TITLE>
<LINK REL=STYLESHEET HREF="/minhnd.servlet.jsp/chap16/el/JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Conditional Evaluation</TH>
    </TR>
  </TABLE>
  <P>
  <TABLE BORDER=1 ALIGN="CENTER">
    <TR>
      <TH>
      <TH CLASS="COLORED">Apples</TH>
      <TH CLASS="COLORED">Oranges</TH>
    </TR>
    <TR>
      <TH CLASS="COLORED">First Quarter</TH>
      <TD ALIGN="RIGHT">${apples.q1}</TD>
      <TD ALIGN="RIGHT">${oranges.q1}</TD>
    </TR>
    <TR>
      <TH CLASS="COLORED">Second Quarter</TH>
      <TD ALIGN="RIGHT">${apples.q2}</TD>
      <TD ALIGN="RIGHT">${oranges.q2}</TD>
    </TR>
    <TR>
      <TH CLASS="COLORED">Third Quarter</TH>
      <TD ALIGN="RIGHT">${apples.q3}</TD>
      <TD ALIGN="RIGHT">${oranges.q3}</TD>
    </TR>
    <TR>
      <TH CLASS="COLORED">Fourth Quarter</TH>
      <TD ALIGN="RIGHT">${apples.q4}</TD>
      <TD ALIGN="RIGHT">${oranges.q4}</TD>
    </TR>
    <TR>
      <TH CLASS="COLORED">Total</TH>
      <TD ALIGN="RIGHT" BGCOLOR="${(apples.total < 0) ? 'RED' : 'WHITE' }">${apples.total}</TD>
      <TD ALIGN="RIGHT" BGCOLOR="${(oranges.total < 0) ? 'RED' : 'WHITE' }">${oranges.total}</TD>
    </TR>
  </TABLE>
</BODY>
</HTML>