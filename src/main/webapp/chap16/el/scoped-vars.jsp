<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>Accessing Scoped Variables</TITLE>
<LINK REL=STYLESHEET HREF="/minhnd.servlet.jsp/chap16/el/JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Accessing Scoped Variables</TH>
    </TR>
  </TABLE>
  <P>
  <UL>
    <LI><B>attribute1:</B> ${attribute1}</LI>
    <LI><B>attribute2:</B> ${attribute2}</LI>
    <LI><B>attribute3:</B> ${attribute3}</LI>
    <LI><B>Source of "repeated" attribute:</B> ${repeated}</LI>
  </UL>
</BODY>
</HTML>