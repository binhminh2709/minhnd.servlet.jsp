<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>Accessing Collections</TITLE>
<LINK REL=STYLESHEET HREF="/minhnd.servlet.jsp/chap16/el/JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Accessing Collections</TH></TR>
  </TABLE>
  <P>
  <UL>
    <LI>${first[0]}${last[0]} (${company["Ellison"]})</LI>
    <LI>${first[1]}${last[1]} (${company["Gates"]})</LI>
    <LI>${first[2]}${last[2]} (${company["McNealy"]})</LI>
  </UL>
</BODY>
</HTML>