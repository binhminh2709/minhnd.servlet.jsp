<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<HTML>
<HEAD>
<TITLE>Using Implicit Objects</TITLE>
<LINK REL=STYLESHEET HREF="/minhnd.servlet.jsp/chap16/el/JSP-Styles.css" TYPE="text/css">
</HEAD>
<BODY>
  <TABLE BORDER=5 ALIGN="CENTER">
    <TR>
      <TH CLASS="TITLE">Using Implicit Objects</TH>
    </TR>
  </TABLE>
  <P>
  <UL>
    <LI><B>test Request Parameter:</B> ${param.test}</LI>
    <LI><B>User-Agent Header:</B> ${header["User-Agent"]}</LI>
    <LI><B>JSESSIONID Cookie Value:</B> ${cookie.JSESSIONID.value}</LI>
    <LI><B>Server:</B> ${pageContext.servletContext.serverInfo}</LI>
  </UL>
</BODY>
</HTML>