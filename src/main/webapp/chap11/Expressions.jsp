<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 TRANSITIONAL//EN">
<!-- 
EXAMPLE OF JSP EXPRESSIONS. 
   
TAKEN FROM CORE SERVLETS AND JAVASERVER PAGES 2ND EDITION
FROM PRENTICE HALL AND SUN MICROSYSTEMS PRESS,
HTTP://WWW.CORESERVLETS.COM/.
(C) 2003 MARTY HALL; MAY BE FREELY USED OR ADAPTED.
-->
<html>
<head>
<title>jsp expressions</title>
<meta name="keywords" content="jsp,expressions,javaserver pages,servlets">
<meta name="description" content="a quick example of jsp expressions.">
<link rel=stylesheet
      href="jsp-styles.css"
      type="text/css">
</head>
<body>
<h2>jsp expressions</h2>
<ul>
  <LI>CURRENT TIME: <%= new java.util.Date() %>
  <LI>SERVER: <%= application.getServerInfo() %>
  <LI>SESSION ID: <%= session.getId() %>
  <LI>THE <CODE>TESTPARAM</CODE> FORM PARAMETER:<%= request.getParameter("testParam") %>
</UL>
</BODY></HTML>